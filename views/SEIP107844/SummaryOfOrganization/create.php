<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <form action="store.php" method="POST">
            <fieldset>
                <legend>Add New Organization </legend>
                <div>
                    <label>Enter Organization Name:</label>
                    <input 
                        autofocus=""
                        type="text"
                        name="organization"
                        id="name"
                        tabindex="1"
                        required="required"
                        placeholder="Enter organization name"
                    >
                </div>
                <div>
                    <label>Enter the title of Organization :</label>
                    <input 
                        type="text"
                        name="title"
                        id="name"
                        tabindex="1"
                        required="required"
                        placeholder="Enter organization name"
                    >
                </div>
                <div>
                    <button type="submit">Save</button>
                    <button type="button">Save and Add Again</button>
                    <button type="reset">Reset</button>
                </div>
            </fieldset>
        </form>
        <div>
            <li><a href="index.php">Go to List</a></li>
            <li><a href="../../../index.php">Home</a></li>
            <li><a href="Javascript:history.go(-1)">Back</a></li>
        </div>
    </body>
</html>


