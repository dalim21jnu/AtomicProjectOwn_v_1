<?php
    ini_set("display_errors", "Off");
    include_once("../../../vendor/autoload.php");
    use App\Bitm\SEIP107844\SummaryOfOrganization\Organization;
    $organization = new Organization();
    $organizations = $organization->index();
?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            #utility{
                float:right;
                width:65%;
            }
        </style>
    </head>
    <body>
        <h1>List of Organization</h1>
        <div><a href="../../../index.php">Home</a></div>
        <div>
            <span>Search/Filter</span>
            <select>
                <option>10</option>
                <option>20</option>
                <option>30</option>
                <option>40</option>
                <option>50</option>
            </select>
            <span id="utility">Download as PDF | XL |<a href="create.php">Add New Organization</a></span>
        </div>
        <table border="1">
            <thead>
                <tr>
                    <th>SI.</th>
                    <th>Organization Name &dArr;</th>
                    <th>Summary of Organization &dArr;</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody >
                <?php 
                $sino = 1;
                foreach ($organizations as $organization) {
                 ?>
                <tr>
                    <th><?php echo $sino;?></th>
                    <th><?php echo $organization->organization;?></th>
                    <th><?php echo $organization->title;?></th>
                    <th>View | Edit|Trash/Recover |Email to Friend <th>
                </tr>
                <?php   
                $sino++;
                }?>
                
            </tbody>
        </table>
        <div><span> previous<< 1|2|3|4|5 >> next  </span></div>
    </body>
</html>


