<?php
//Very Very Important function
/*error_reporting(-1);
ini_set('display_errors', 'On');
set_error_handler("var_dump");*/

include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjectOwn'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
use App\Bitm\SEIP107844\Utility\Utility;
use App\Bitm\SEIP107844\ProfilePicture\Picture;
$profile = new Picture($_POST);
$pics = $profile->index();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Profile Picture Page</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            #utility{
                float:right;
                width: 70%;
            }
            #message{
                background-color: green;
            }
        </style>
    </head>
    <body>
        <h1>List of Profile Picture</h1>
        <div id="message">
        <?php echo Utility::message(); ?>
        </div>
        <div>
           <span>Search/Filter</span>
           <select>
               <option>10</option>
               <option>20</option>
               <option>30</option>
               <option>40</option>
               <option>50</option>
           </select>
           <span id="utility">Download as PDF | XL | <a href="create.php">Add New</a></span>
        </div>
        
        <table border = "1">
            <thead>
                <tr>
                    <th>SI</th>
                    <th>Name</th>
                    <th>Profile Picture</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $sino = 1;
                foreach ($pics as $pictures) { ?>
                    <tr>
                        <td><?php echo $sino; ?></td>
                        <td><?php echo $pictures->name; ?></td>
                        <td><img src="<?php echo $pictures->img_path; ?>"></td>
                        <td><form action="show.php" method="post">
                                <input name="id" type="hidden" value="<?php echo $pictures->id;?>" >
                                <button type="submit">View</button>
                            </form>|Edit
                            |<form action="delete.php" method="post">
                                <input name="id" type="hidden" value="<?php echo $pictures->id;?>" >
                                <button type="submit" class="delete">Delete</button>
                            </form>
                            |Trash/Recover|Email to friend<td>
                    </tr>
                <?php
                $sino++;
                } ?>
                    
            </tbody>
        </table>
        <div><span>previous 1 | 2 | 3 | 4 | 5 next</span></div>
        <script type="text/javascript" src="../../../resource/bootstrap/jquery-2.1.4.min.js"></script>
        <script>
            $(".delete").bind("click",function(e){
                var deleteItem = confirm("Are you sure you want to delete?");
                if(!deleteItem){
                    e.preventDefault();
                }
            });
            
            $("#message").hide(3500);

        </script>
    </body>
</html>

