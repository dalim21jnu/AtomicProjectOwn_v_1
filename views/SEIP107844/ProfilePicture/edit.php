<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjectOwn'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php'); 
//include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjectOwn'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
use App\Bitm\SEIP107844\ProfilePicture\Picture;
$profile = new Picture($_POST);
$profiles = $profile->show();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Create</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <form action="update.php" method="post" enctype="multipart/form-data">
            <fieldset>
                <legend>Edit Profile Picture</legend>
                <div>
                    <label for="name">Enter User Name</label>
                    <input type="text" 
                           name="username" 
                           autofocus=""
                           required="required" 
                           placeholder="Enter user name"  
                           tabindex="1"
                        >
                </div>
                     <label for="image">Edit Profile Picture</label>
                     <input type="file" name="img_file" value="Upload Image" tabindex="2">
                </div>
                <div>
                    <button type="Submit" tabindex="3">Save</button>
                    <button type="Submit" tabindex="4">Save & Add Again</button>
                    <button type="reset"  tabindex="5">Reset</button>
                </div>
                
            </fieldset>
        </form>
        <nav>
            <li><a href="index.php">Go to List</a></li>
            <li><a href="../../../index.php">Home</a></li>
            <li><a href="Javascript:history.go(-1)">Back</a></li>
        </nav>
    </body>
</html>


