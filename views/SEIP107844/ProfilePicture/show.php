<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjectOwn'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php'); 
//include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjectOwn'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
use App\Bitm\SEIP107844\ProfilePicture\Picture;
$profile = new Picture($_POST);
$profiles = $profile->show();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Profile Picture Page</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1>Profile Pictur Details:</h1>
        <div>
            <dl>
                <dt>SI</dt>
                <dd><?php echo $profiles->id; ?></dd>
                <dt>User Name</dt>
                <dd><?php echo $profiles->name; ?></dd>
                <dt>Profile Picture</dt>
                <dd><img src="<?php echo $profiles->img_path; ?>" ></dd>
            </dl>
        </div>
        <nav>
            <li><a href="index.php">Go to List</a></li>
        </nav>
    </body>
</html>
