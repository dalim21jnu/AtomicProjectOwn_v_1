<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjectOwn'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
use \App\Bitm\SEIP107844\EmailSubscription\Email;    
use App\Bitm\SEIP107844\Utility\Utility;
$email = new Email();
$emails = $email->index();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Email Page</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            #utility{
                float:right;
                width: 70%;
            }
            #message{
                background-color: green;
            }
          
        </style>
    </head>
    <body>
        <h1>List of Email Subscription</h1>
        <div id="message">
             <?php echo Utility::message();?>
        </div>
        <div>
           <span>Search/Filter</span>
           <select>
               <option>10</option>
               <option>20</option>
               <option>30</option>
               <option>40</option>
               <option>50</option>
           </select>
           <span id="utility">Download as PDF | XL | <a href="create.php">Add New</a></span>
        </div>
        
        <table border = "1">
            <thead>
                <tr>
                    <th>SI</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $sino = 1;
                foreach ($emails as $email)
                    {  
                ?>
                    <tr>
                        <td><?php echo $sino; ?></td>
                        <td><?php echo $email->username; ?></td>
                        <td><?php echo $email->emailaddress; ?></td>
                        <td>View|Edit
                            | <form action="delete.php" method="post">
                                <input type="hidden" name="id" value="<?php echo $email->id; ?>" class="delete">
                                <button type="submit">Delete</button>
                            </form>
                            |Trash/Recover|Email to friend<td>
                    </tr>
                <?php
                $sino++;
                 } 
                ?>
              
            </tbody>
        </table>
        <div><span>previous 1 | 2 | 3 | 4 | 5 next</span></div>
        <script src="../../../resource/bootstrap/jquery-2.1.4.min.js" type="text/javascript"></script>
        <script>
                $('.delete').bind('click',function(e){
                var deleteItem = confirm('Are you sure you want to delete?');
                if(!deleteItem){
                    e.preventDefault();
                }
            });
            $('#message').hide(1500);
        </script>
    </body>
</html>
