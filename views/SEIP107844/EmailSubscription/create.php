<!DOCTYPE html>
<html>
    <head>
        <title>Create Page</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
    </head>
    <body>
        <form action="store.php" method="POST">
            <fieldset>
                <legend>Add New Email</legend>            
                
                <div>
                    <label for="name">Enter User Name: </label>
                    <input 
                        id ="name"
                        autofocus=""
                        type="text" 
                        name="username" 
                        tabindex="1"
                        placeholder="Enter User name"
                        required="required"
                    >
                </div>
                
                <div>
                        <label for="email">Enter Your Email Address: </label>
                    <input 
                        id ="address"
                        type="text" 
                        name="emailaddress" 
                        tabindex="2"
                        placeholder="Enter your email"
                        required="required"
                    >
                </div>
                
                <div>
                    <button type="submit" tabindex="3">Save</button>
                    <button type="button" tabindex="4">Save & Add</button>
                    <button type="reset" tabindex="4">Reset</button>
                </div>       
            </fieldset>
        </form>
        <nav>
            <li><a href="index.php">Go to List</a></li>
            <li><a href="../../../index.php">Home</a></li>
            <li><a href="Javascript:history.go(-1)">Back</a></li>
        </nav>
    </body>
</html>


