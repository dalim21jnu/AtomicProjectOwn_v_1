<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <form action="store.php" method="post">
            <fieldset>
                <legend>Select City</legend>
                <div>
                    <label>Select City:</label>
                    <select name="city" > 
                        
                        <optgroup label="Australia">
                            <option value="sydney">Sydney</option>
                            <option value="melbourne">Melbourne</option>
                            <option value="brisbane">Brisbane</option>
                        </optgroup>
                        <optgroup label="Bangladesh">
                            <option value="dhaka">Dhaka</option>
                            <option value="chittagong">Chittagong</option>
                            <option value="khulna">Khulna</option>
                        </optgroup>
                        <optgroup label="New Zealand">
                            <option value="auckland">Auckland</option>
                            <option value="dunedin">Dunedin</option>
                            <option value="nelson">Nelson</option>
                        </optgroup>
                        <optgroup label="Thailand">
                            <option value="bangkok">Bangkok</option>
                            <option value="mai">Chiang Mai</option>
                            <option value="pattaya">Pattaya</option>
                        </optgroup>
                    </select>
                <div>
                    <button type="submit">Save</button>
                    <button type="button">Save and Add again</button>
                    <button type="reset">Reset</button>
                </div>
            </fieldset>
        </form>
    </body>
</html>

