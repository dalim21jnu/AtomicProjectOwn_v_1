<?php
    ini_set("display_errors", "Off");
    include_once("../../../vendor/autoload.php");
    use App\Bitm\SEIP107844\SelectCity\City;
    $city = new City($_POST);
    $cities = $city->index($city);
?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            #utility{
                float: right;
                width: 80%
            }
        </style>
    </head>
    <body>
        <h1>List of City</h1>
        <div><a href="../../../index.php">Home</a></div>
        <div>
            <span>Search/Filter</span>
            <select>
                <option>15</option>
                <option>30</option>
                <option>45</option>
                <option>60</option>
                <option>75</option>
            </select>
            <span id="utility">Download as PDF | XL |<a href="create.php"> Add New City</a></span>
        </div>
        <table border="1">
            <thead>
                <tr>
                    <th>SI</th>
                    <th>City</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $sino = 1;
                foreach ($cities as $city) {
                ?>
                    <tr>
                        <td><?php echo $sino; ?></td>
                        <td><?php echo $city->city;?></td>
                        <td>View | Edit| Delete |Trash/Recovery | Email to Friends<td>
                    </tr>
                <?php  
                $sino++;
                } ?>
            </tbody>
        </table>
        <div><span>previous << 1|2|3|4|5 >>next</span></div>
    </body>
</html>


