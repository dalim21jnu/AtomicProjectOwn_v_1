<?php 
    include_once("../../../vendor/autoload.php");
    use App\Bitm\SEIP107844\RadioGender\Gender;
    $gender = new Gender();
    $genders = $gender->index();
?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            #utility{
                float: right;
                width: 80%;
            }
        </style>
    </head>
    <body>
        <h1>List of Gender</h1>
        <div><a href="../../../index.php">Home</a></div>
        <div>
            <span>Search/Filter</span>
            <select>
                <option>15</option>
                <option>30</option>
                <option>45</option>
                <option>60</option>
                <option>75</option>
            </select>
            <span id="utility">Download as PDF | XL |<a href="create.php"> Add New Gender</a></span>
        </div>
        <table border="1">
            <thead>
                <tr>
                    <th>SI</th>
                    <th>Name of Persons</th>
                    <th>Gender</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php  
                $sino = 1;
                foreach ($genders as $gender) {
                ?>
                    <tr>
                        <td><?php echo $sino; ?></td>
                        <td><?php echo $gender->name; ?></td>
                        <td><?php echo $gender->gender; ?></td>
                        <td>View | Edit| Delete |Trash/Recovery | Email to Friends<td>
                    </tr>
                <?php
                $sino++;
                }
                ?>
            </tbody>
        </table>
        <div><span>previous << 1|2|3|4|5 >>next</span></div>
    </body>
</html>
