<?php
    include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjectOwn'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');   
    use App\Bitm\SEIP107844\BirthdayDate\Birthday;
    use App\Bitm\SEIP107844\Utility\Utility;
    $birth = new Birthday();
    $birthdaydates = $birth->show($_GET['id']);
    //Utility::debug($birthdaydates);
?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            #utility{
                float:right;
                width:80%;
            }
            #message{
                background-color: green;
            }
        </style>
    </head>
    <body>
        <h1>Birthday Date Details</h1>
        <dl>
            <dt>SI<dt>
            <dd><?php echo $birthdaydates->id;?></dd>
            <dt>Name<dt>
            <dd><?php echo $birthdaydates->birthdayboy;?></dd>
            <dt>Birthday Date<dt>
            <dd><?php echo $birthdaydates->birthdaydate;?></dd>
        </dl>
    </body>
    <nav>
        <ul>
            <li><a href="index.php">Go to List</a></li>
        </ul>
    </nav>
</html>