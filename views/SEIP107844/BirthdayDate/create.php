<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <form action="store.php" method="POST">
            <fieldset>
                <legend>Add New Birthday Date</legend>
                <div>
                    <label>Enter Name:</label>
                    <input 
                        autofocus=""
                        type="text"
                        name="birthdayboy"
                        id="name"
                        tabindex="1"
                        required="required"
                        placeholder="Enter user name"
                    >
                </div>
                <div>
                    <label>Enter Birthday Date:</label>
                    <input 
                        type="date"
                        name="birthdaydate"
                        id="name"
                        tabindex="2"
                        required="required"
                        placeholder="YY-Mo-Day"
                    >
                </div>
                <div>
                    <button type="submit">Save</button>
                    <button type="button">Save and Add Again</button>
                    <button type="reset">Reset</button>
                </div>
            </fieldset>
        </form>
        <div>
            <li><a href="index.php">Go to List</a></li>
            <li><a href="../../../index.php">Home</a></li>
            <li><a href="Javascript:history.go(-1)">Back</a></li>
        </div>
    </body>
</html>

