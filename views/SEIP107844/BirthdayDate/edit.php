<?php
    include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjectOwn'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');   
    use App\Bitm\SEIP107844\BirthdayDate\Birthday;
    use App\Bitm\SEIP107844\Utility\Utility;
    $birth = new Birthday();
    $birthdaydates = $birth->show($_GET['id']);
    //Utility::debug($birthdaydates);
?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <form action="update.php" method="POST">
            <fieldset>
                <legend>Edit Birthday Date</legend>
                    <input 
                        type="hidden"
                        name="id"
                        value="<?php echo $birthdaydates->id;?>"
                    >
                <div>
                    <label>Edit Name:</label>
                    <input 
                        autofocus=""
                        type="text"
                        placeholder="Enter your name"
                        name="birthdayboy"
                        required="required"
                        value="<?php echo $birthdaydates->birthdayboy;  ?>"
                    >
                </div>
                <div>
                    <label>Edit Birthday Date:</label>
                    <input 
                        type="date"
                        name="birthdaydate"
                        placeholder="YY-Mo-Day"
                        required="required"
                        value="<?php echo $birthdaydates->birthdaydate;  ?>"
                    >
                </div>
                <div>
                    <button type="submit">Save</button>
                    <button type="button">Save and Add Again</button>
                    <button type="reset">Reset</button>
                </div>
            </fieldset>
        </form>
        <div>
            <li><a href="index.php">Go to List</a></li>
            <li><a href="../../../index.php">Home</a></li>
            <li><a href="Javascript:history.go(-1)">Back</a></li>
        </div>
    </body>
</html>

