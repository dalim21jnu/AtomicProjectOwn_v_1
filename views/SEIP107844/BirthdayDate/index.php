<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjectOwn'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');   
use App\Bitm\SEIP107844\BirthdayDate\Birthday;
use App\Bitm\SEIP107844\Utility\Utility;
$birth = new Birthday();
$birthdaydates = $birth->index();
?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            #utility{
                float:right;
                width:80%;
            }
            #message{
                background-color: green;
            }
            .delete{
                
            }
        </style>
    </head>
    <body>
        <h1>List of Birthday Date</h1>
        <div><a href="../../../index.php">Home</a></div>
        <div id="message">
            <?php echo Utility::message(); ?>
        </div>
        <div>
            <span>Search/Filter</span>
            <select>
                <option>10</option>
                <option>20</option>
                <option>30</option>
                <option>40</option>
                <option>50</option>
            </select>
            <span id="utility">Download as PDF | XL |<a href="create.php">Add New Birthday</a></span>
        </div>
        <table border="1">
            <thead>
                <tr>
                    <th>SI.</th>
                    <th>Name</th>
                    <th>Birthday Date</th> 
                    <th>Action</th>
                </tr>
            </thead>
            <tbody >
                <?php
                $sino = 1; 
                foreach ($birthdaydates as $birth){  
                ?>
                    <tr>
                        <td><?php echo $sino;?></td>
                        <td><a href="show.php?id=<?php echo $birth->id; ?>"><?php echo $birth->birthdayboy; ?></a></td>
                        <td><?php echo $birth->birthdaydate; ?></td>
                        
                        <td><a href="show.php?id=<?php echo $birth->id; ?>">View</a>
                            |<a href="edit.php?id=<?php echo $birth->id; ?>"> Edit</a>
                            |<a href="delete.php?id=<?php echo $birth->id; ?>" class="delete" >Delete</a>
                       |<form action="delete.php" method="post">
                        <input type="hidden" name="id" value="<?php echo $birth->id; ?>">
                        <button type="submit" class="delete" >Delete</button>    
                        </form>
                            
                        |Trash/Recover |Email to Friend <th>
                    </tr>
                <?php
                $sino++;  
                } 
                ?>
               
            </tbody>
        </table>
        <div><span> previous<< 1|2|3|4|5 >> next  </span></div>
        <script src="../../../resource/bootstrap/jquery-2.1.4.min.js" type="text/javascript"></script>
        </script>
            
        <script>
              $('.delete').bind('click',function(e){
                  var deleteItem = confirm('Are you Sure you want to delete?');
                  if(!deleteItem){
                     e.preventDefault();
                  }
              });
              $('#message').hide(3000);
              
        </script>
    </body>
</html>


