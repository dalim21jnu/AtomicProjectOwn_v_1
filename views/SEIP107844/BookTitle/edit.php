<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjectOwn'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php'); 
//ini_set('display_errors', 'Off');
//include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjectOwn'.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php');//best practice using absolute path
use \App\Bitm\SEIP107844\BookTitle\Book;
use \App\Bitm\SEIP107844\Utility\Utility;

$obj = new Book();
$book=$obj->show($_GET['id']);
//Utility::dd($book);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Create</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <form action="update.php" method="post">
            <fieldset>
                <legend>Edit Book </legend>
 
                <input type="hidden" name="id" value="<?php echo $book->id; ?>">
                <div>
                    <label for="title">Enter Book Title</label>
                    <input autofocus=""
                           id="title" 
                           type="text"
                           name="title"
                           placeholder="Enter the title of your book"
                           tabindex="1"
                           required="required"
                           value="<?php echo $book->title; ?>"
                           >
                </div>
                     <label for="author">Enter Author name</label>
                    <input 
                           id="author" 
                           type="text"
                           name="author"
                           placeholder="Enter author name of your book"
                           tabindex="2"
                           required="required"
                           value="<?php echo $book->author; ?>"
                           >
                </div>
                <div>
                    <button type="Submit" tabindex="3">Save</button>
                    <button type="Submit" tabindex="4">Save & Add Again</button>
                   <!-- <button type="Button" >Save & Add</button> -->
                    <button type="reset"  tabindex="5">Reset</button>
                </div>
                
            </fieldset>
        </form>
        <nav>
            <li><a href="index.php">Go to List</a></li>
            <li><a href="../../../index.php">Home</a></li>
            <li><a href="Javascript:history.go(-1)">Back</a></li>
        </nav>
    </body>
</html>


