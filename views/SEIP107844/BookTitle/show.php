<?php
ini_set('display_errors', 'Off');
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjectOwn'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php'); 
//include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjectOwn'.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php');//best practice using absolute path
use \App\Bitm\SEIP107844\BookTitle\Book;
use \App\Bitm\SEIP107844\Utility\Utility;

$obj = new Book();
$book=$obj->show($_GET['id']);
//Utility::dd($book);
?>
 <!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
    <h1>Book Details:</h1>
    <dl>
        <dt>ID:</dt>
        <dd><?php echo $book->id; ?></dd>
        <dt>Book Title:</dt>
        <dd><?php echo $book->title; ?></dd>
        <dt>Author:</dt>
        <dd><?php echo $book->author; ?></dd>
    </dl>
    <nav>
        <ul>
            <li><a href="index.php">Go to List</a></li>
        </ul>
    </nav>
    </body>
</html>

