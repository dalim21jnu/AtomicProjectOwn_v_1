<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Atomic Project</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <h1>BITM - Web App Dev - PHP</h1>
        <dl>
            <dt>Name:</dt>
            <dd>Mohammad Dalim</dd>
        
            <dt>SEIP ID:</dt>
            <dd>SEIP107844</dd>
        
            <dt>Batch:</dt>
            <dd> 10</dd>
        </dl>
        <h2>My Amotmic Project</h2>
        <table>
            <thead>
                <tr>
                    <th>SI.</th>
                    <th>Project Name</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>01</td>
                    <td><a href="views/SEIP107844/Mobile/index.php">Mobile</a></td>
                </tr>
                <tr>
                    <td>02</td>
                    <td><a href="views/SEIP107844/BookTitle/index.php">Book Title</a></td>
                </tr>
                <tr>
                    <td>03</td>
                    <td><a href="views/SEIP107844/BirthdayDate/index.php">Birthday Date</a></td>
                </tr>
                <tr>
                    <td>04</td>
                    <td><a href="views/SEIP107844/SummaryOfOrganization/index.php">Summary of Organization</a></td>
                </tr>
                <tr>
                    <td>05</td>
                    <td><a href="views/SEIP107844/EmailSubscription/index.php">Email: Subscription</a></td>
                </tr>
                <tr>
                    <td>06</td>
                    <td><a href="views/SEIP107844/ProfilePicture/index.php">Profile Picture</a></td>
                </tr>
                <tr>
                    <td>07</td>
                    <td><a href="views/SEIP107844/RadioGender/index.php">Radio: Gender</a></td>
                </tr>
                <tr>
                    <td>08</td>
                    <td><a href="#">Education Level</a></td>
                </tr>
                <tr>
                    <td>09</td>
                    <td><a href="#">Terms and Conditions</a></td>
                </tr>
                <tr>
                    <td>10</td>
                    <td><a href="views/SEIP107844/Hobby/index.php">Hobby</a></td>
                </tr>
                <tr>
                    <td>11</td>
                    <td><a href="views/SEIP107844/SelectCity/index.php">Select: City</a></td>
                </tr>
            </tbody>
        </table>
        
    </body>
</html>
