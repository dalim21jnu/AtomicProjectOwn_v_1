<?php
namespace App\Bitm\SEIP107844\Utility;
//session_start();

class Utility {
    public $message ="";
    static public function debug($param=FALSE) {
        echo "<pre>";
        var_dump($param);
        echo "</pre>";
    }
    static public function dd($param=FALSE) {
        self::debug($param);
        die();
    }
    static public function redirect($url="/AtomicProjectOwn/views/SEIP107844/BookTitle/index.php") {
        header("location:".$url);
    }
   
    static public function message($message = NULL) {
        if(is_null($message)){
            $_message = self::getMessage();
            return $_message;
        }  else {
            self::setMessage($message);
        }
    }
    static private function getMessage() {
        $_message = $_SESSION['message'];
        $_SESSION['message'] = NULL;
        return $_message;
    }
    static private function setMessage($message) {
        $_SESSION['message']=$message;
    }
     /*static public function message($message = NULL) {
        if($message){
            $_message = $_SESSION['message'];
            $_SESSION['message'] = NULL;
            return $_message;
        }else{
            $_SESSION['message']=$message;
        }
    }*/
}
