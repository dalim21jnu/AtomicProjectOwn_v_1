<?php
namespace App\Bitm\SEIP107844\BookTitle;
use \App\Bitm\SEIP107844\Utility\Utility;
class Book {
    public $id = "";
    public $title = "";
    public $author = "";
//    public $created = "";
//    public $modified = "";
//    public $created_by = "";
//    public $modified_by = "";
//    public $deleted_at = "";

    public function __construct($data = FALSE) {
        if(is_array($data) &&array_key_exists('id', $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        $this->title = $data['title'];
        $this->author = $data['author'];
    }
    public function show($id = FALSE) {
        $conn = mysql_connect("localhost", "root", "");
        $link = mysql_select_db("atomicprojectown");
        $query = "SELECT * FROM `books` WHERE id =".$id;
        $result = mysql_query($query);
        $row = mysql_fetch_object($result);
        return $row;
    }
    public function index() {
        $books = array();
        $conn = mysql_connect("localhost", "root", "");
        $link = mysql_select_db("atomicprojectown");
        $query = "SELECT * FROM `books`";
        $result = mysql_query($query);
        while ($row = mysql_fetch_object($result)) {
            $books[] = $row;
        }
        return $books;
    }
    public function store() {
        $conn = mysql_connect("localhost", "root", "");
        $link = mysql_select_db("atomicprojectown");
        //$query="INSERT INTO `atomicprojectown`.`books` (`title`) VALUES ('$this->title')";
        $query="INSERT INTO `atomicprojectown`.`books` (`title`, `author`) VALUES ('$this->title','$this->author')";
        $result = mysql_query($query);
        if($result){
            Utility::message("Book title is added successfully");
        }else{
            Utility::message("There is an error while saving data.Please try again later");
        }
        Utility::redirect();
    }

    public function delete($id = NULL) {
        if(is_null($id)){
            Utility::message("id is not abailable");
            return Utility::redirect('index.php');
        }
        $conn = mysql_connect("localhost", "root", "");
        $link = mysql_select_db("atomicprojectown");
        $query ="DELETE FROM `atomicprojectown`.`books` WHERE `books`.`id` = ".$id;
        $result = mysql_query($query);
        if($result){
            Utility::message("Book title is deleted successfully");
        }else{
            Utility::message("Cannot delete");
        }
        Utility::redirect();
    }
    public function update() {
        $conn = mysql_connect("localhost", "root", "");
        $link = mysql_select_db("atomicprojectown");
        $query = "UPDATE `atomicprojectown`.`books` SET `title` = '$this->title', `author` = '$this->author' WHERE id=".$this->id;
        $result = mysql_query($query);
        if($result){
            Utility::message("Book title is Edited successfully");
        }else{
            Utility::message("There is an error while Editing data.Please try again later");
        }
        Utility::redirect();
    }

}
